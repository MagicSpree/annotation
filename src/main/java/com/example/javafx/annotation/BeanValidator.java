package com.example.javafx.annotation;

import com.example.javafx.annotation.impl.ValidationResult;

public interface BeanValidator {
    ValidationResult validate(Object bean);
    void setValue(Object bean);
}

package com.example.javafx.annotation.impl;

import com.example.javafx.annotation.*;
import com.example.javafx.annotation.exception.ValidationException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class BasedBeanValidation implements BeanValidator {

    private final Map<Class<? extends Annotation>, FieldValidator> validatorMap;
    private final Set<Class<? extends Annotation>> supportedAnnotation;

    public BasedBeanValidation(Map<Class<? extends Annotation>, FieldValidator> validatorMap) {
        this.validatorMap = validatorMap;
        this.supportedAnnotation = this.validatorMap.keySet();
    }

    @Override
    public ValidationResult validate(Object bean) {

        ValidationResult validationResult = new ValidationResult();

        if (bean == null) {
            throw new ValidationException("Passed bean is null");
        }

        Class<?> clazz = bean.getClass();
        if (!clazz.isAnnotationPresent(ValidBean.class)) {
            String msg = MessageFormat.format("{0} does not have the {1} annotation.",
                    clazz, ValidBean.class.getName());
            throw new ValidationException(msg);
        }

        Field[] fields = clazz.getDeclaredFields();
        for (var field : fields) {
            field.setAccessible(true);
            List<BrokenField> brokenFields = supportedAnnotation.stream()
                    .filter(field::isAnnotationPresent)
                    .map(validatorMap::get)
                    .map(fieldValidator -> fieldValidator.validate(bean, field))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            validationResult.addBrokenFields(brokenFields);
        }
        return validationResult;
    }

    @Override
    public void setValue(Object bean) {
        if (bean == null) {
            throw new ValidationException("Passed bean is null");
        }
        Class<?> clazz = bean.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (var field : fields) {

            if (field.isAnnotationPresent(DefaultValue.class)) {
                DefaultValue defaultValue = field.getAnnotation(DefaultValue.class);
                try {
                    field.setAccessible(true);
                    field.set(bean, defaultValue.value());
                } catch (IllegalAccessException e) {
                    throw new ValidationException(e);
                }
            }

        }
    }
}

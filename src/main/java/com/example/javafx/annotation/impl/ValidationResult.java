package com.example.javafx.annotation.impl;

import com.example.javafx.annotation.impl.BrokenField;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    private List<BrokenField> brokenFields = new ArrayList<>();

    public void addBrokenFields(List<BrokenField> brokenFields) {
        this.brokenFields.addAll(brokenFields);
    }

    public List<BrokenField> getBrokenFields() {
        return new ArrayList<>(brokenFields);
    }

}

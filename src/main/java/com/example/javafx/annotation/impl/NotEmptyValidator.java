package com.example.javafx.annotation.impl;

import com.example.javafx.annotation.FieldValidator;

import java.lang.reflect.Field;

public class NotEmptyValidator implements FieldValidator {

    @Override
    public BrokenField validate(Object entity, Field field) {
        if (String.class.isAssignableFrom(field.getType())) {
            try {
                String fieldValue = (String) field.get(entity);
                if (fieldValue != null && fieldValue.isEmpty()) {
                    return new BrokenField(field.getName(), fieldValue, "Это поле не может быть пустым");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

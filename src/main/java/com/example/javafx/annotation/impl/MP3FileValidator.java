package com.example.javafx.annotation.impl;

import com.example.javafx.annotation.FieldValidator;
import com.example.javafx.annotation.MP3File;

import java.lang.reflect.Field;
import java.util.regex.Pattern;


public class MP3FileValidator implements FieldValidator {

    @Override
    public BrokenField validate(Object entity, Field field) {
        if (String.class.isAssignableFrom(field.getType())) {
            MP3File mp3File = field.getAnnotation(MP3File.class);
            String regex = mp3File.regex();
            Pattern pattern = Pattern.compile(regex);
            try {
                String fieldValue = (String) field.get(entity);
                if (fieldValue != null
                        && !fieldValue.isEmpty()
                        && !pattern.matcher(fieldValue).matches()) {
                    BrokenField brokenField = new BrokenField(field.getName(), fieldValue, "Это не MP3 файл");
                    return brokenField;
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

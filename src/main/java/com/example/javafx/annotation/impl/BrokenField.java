package com.example.javafx.annotation.impl;


/**
 * "Сломанное"/невалидное поле
 */
public class BrokenField {

    private final String fieldName;
    private final String fieldValue;
    private final String violatedRule;


    public BrokenField(String fieldName, String fieldValue, String violatedRule) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.violatedRule = violatedRule;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public String getViolatedRule() {
        return violatedRule;
    }
}

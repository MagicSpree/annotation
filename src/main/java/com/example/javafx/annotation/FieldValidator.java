package com.example.javafx.annotation;

import com.example.javafx.annotation.impl.BrokenField;

import java.lang.reflect.Field;

public interface FieldValidator {
    BrokenField validate(Object entity, Field field);
}

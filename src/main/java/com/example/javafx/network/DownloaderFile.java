package com.example.javafx.network;

import com.example.javafx.observer.LiveData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

public class DownloaderFile implements Downloader {

    public LiveData<Response<File>> liveData = new LiveData<>();
    private final Response<File> fileResponse = new Response<>();


    @Override
    public void download(String downloadUrl, String destPath) {

        Thread thread = new Thread(() -> {
            startDownload(downloadUrl, destPath);
            liveData.addValue(fileResponse);
        });
        thread.setDaemon(true);
        thread.start();
    }

    private void startDownload(String downloadUrl, String destPath) {
        try {
            File outputFile = new File(destPath);
            URLConnection downloadFileConnection = addFileResume(downloadUrl, outputFile);
            transferData(downloadFileConnection, outputFile);
            if (!fileResponse.isError()) {
                System.out.println("Скачивание файла " + destPath + " завершено");
                fileResponse.setSuccess(new File(destPath));
            }
        } catch (IOException | URISyntaxException e) {
            fileResponse.setError(e.toString());
        }
    }

    private void transferData(URLConnection downloadFileConnection, File outputFile) {

        try (ReadableByteChannel readableByteChannel = Channels.newChannel(downloadFileConnection.getInputStream());
             FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
             FileChannel fileChannel = fileOutputStream.getChannel()) {

            fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        } catch (IOException e) {
            fileResponse.setError(e.toString());
        }
    }

    private URLConnection addFileResume(String downloadUrl, File outputFile)
            throws IOException, URISyntaxException {

        long existingFileSize;
        URLConnection downloadFileConnection = new URI(downloadUrl).toURL()
                .openConnection();

        if (outputFile.exists() && downloadFileConnection instanceof HttpURLConnection) {
            HttpURLConnection httpFileConnection = (HttpURLConnection) downloadFileConnection;

            HttpURLConnection tmpFileConn = (HttpURLConnection) new URI(downloadUrl).toURL()
                    .openConnection();
            tmpFileConn.setRequestMethod("HEAD");
            long fileLength = tmpFileConn.getContentLengthLong();
            existingFileSize = outputFile.length();

            if (existingFileSize < fileLength) {
                httpFileConnection.setRequestProperty("Range", "bytes=" + existingFileSize + "-" + fileLength);
            } else {
                fileResponse.setSuccess(outputFile);
            }
        }
        return downloadFileConnection;
    }

}

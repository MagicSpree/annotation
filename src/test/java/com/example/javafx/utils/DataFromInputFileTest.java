package com.example.javafx.utils;

import com.example.javafx.annotation.*;
import com.example.javafx.annotation.impl.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class DataFromInputFileTest {

    private final String PATH_TO_TEXT_FILE_VERIFY = "./src/main/resources/testVerify.txt";
    private final String PATH_TO_TEXT_FILE_ERROR = "./src/main/resources/testError.txt";

    @Test
    void checkNotEmptyAndMP3Annotation_ERROR() {
        DataFromInputFile dataFromInputFile = new DataFromInputFile(new File(PATH_TO_TEXT_FILE_ERROR));

        Map<Class<? extends Annotation>, FieldValidator> validatorMap = new HashMap<>();
        validatorMap.put(NotEmpty.class, new NotEmptyValidator());
        validatorMap.put(MP3File.class, new MP3FileValidator());
        BeanValidator beanValidator = new BasedBeanValidation(validatorMap);

        ValidationResult validationResult = beanValidator.validate(dataFromInputFile);
        Assertions.assertNotNull(validationResult);

        List<BrokenField> brokenFields = validationResult.getBrokenFields();
        Assertions.assertEquals(1, brokenFields.size());

        BrokenField brokenField = brokenFields.get(0);
        Assertions.assertEquals("Это не MP3 файл", brokenField.getViolatedRule());
        Assertions.assertEquals("musicUrlString", brokenField.getFieldName());
    }

    @Test
    void checkDefaultValueAnnotation_ERROR() {
        DataFromInputFile dataFromInputFile = new DataFromInputFile(new File(PATH_TO_TEXT_FILE_ERROR));

        Map<Class<? extends Annotation>, FieldValidator> validatorMap = new HashMap<>();
        validatorMap.put(DefaultValue.class, new NotEmptyValidator());
        validatorMap.put(MP3File.class, new MP3FileValidator());
        BeanValidator beanValidator = new BasedBeanValidation(validatorMap);

        beanValidator.setValue(dataFromInputFile);

        Assertions.assertEquals("Empty", dataFromInputFile.getImagePathString());
        Assertions.assertEquals("Empty", dataFromInputFile.getImageUrlString());
    }

    @Test
    void checkAllAnnotation_VERIFY() {
        DataFromInputFile dataFromInputFile = new DataFromInputFile(new File(PATH_TO_TEXT_FILE_VERIFY));

        Map<Class<? extends Annotation>, FieldValidator> validatorMap = new HashMap<>();
        validatorMap.put(NotEmpty.class, new NotEmptyValidator());
        validatorMap.put(MP3File.class, new MP3FileValidator());
        BeanValidator beanValidator = new BasedBeanValidation(validatorMap);

        ValidationResult validationResult = beanValidator.validate(dataFromInputFile);
        beanValidator.setValue(dataFromInputFile);
        Assertions.assertNotNull(validationResult);

        List<BrokenField> brokenFields = validationResult.getBrokenFields();
        Assertions.assertEquals(0, brokenFields.size());

        Assertions.assertEquals("Empty", dataFromInputFile.getImagePathString());
        Assertions.assertEquals("Empty", dataFromInputFile.getImageUrlString());
    }
}